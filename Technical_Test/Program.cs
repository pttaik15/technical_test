﻿using System.Text;

namespace Technical_Test;

public class Program
{
    private static string ChuyenSoThanhChu(int number)
    {
        if (number < 0 || number >= 1000)
            throw new ArgumentOutOfRangeException("Number should be between 0 and 999 inclusive.");

        string[] units = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };

        switch (number)
        {
            case < 10:
                return units[number];
            case < 20:
                return "mười " + (number == 10 ? "" : units[number % 10]);
        }

        var hundreds = number / 100;
        var tens = (number % 100) / 10;
        var ones = number % 10;

        var result = "";

        if (hundreds > 0)
        {
            result += units[hundreds] + " trăm ";
            if (tens == 0 && ones > 0)
                result += "linh ";
        }

        switch (tens)
        {
            case > 1:
            {
                result += units[tens] + " mươi ";
                if (ones == 1)
                    result += "mốt";
                else if (ones > 1)
                    result += units[ones];
                break;
            }
            case 1:
                result += "mười " + units[ones];
                break;
            case 0 when ones > 0:
                result += units[ones];
                break;
        }

        return result.Trim();
    }

    public static void Main()
    {
        Console.OutputEncoding = Encoding.UTF8;
        Console.WriteLine(ChuyenSoThanhChu(101)); // một trăm linh một
        Console.WriteLine(ChuyenSoThanhChu(231)); // hai trăm ba mươi mốt
        Console.WriteLine(ChuyenSoThanhChu(19)); // mười chín
        Console.WriteLine(ChuyenSoThanhChu(10)); // mười
        Console.WriteLine(ChuyenSoThanhChu(0)); // không
    }
}